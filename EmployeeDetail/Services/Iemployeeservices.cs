using EmployeeDetail.Modals;
using System.Collections.Generic;
namespace EmployeeDetail.Services
{
    public interface Iemployeeservices
    {
      IEnumerable<Employee_portal> GetEmployee_portalData();

     

       bool SaveEmployeeData (Employee_portal emp);

        bool UpdateEmployeedetails (Employee_portal emp);

        bool DeleteEmployeeDetails(long empid);
    }
}