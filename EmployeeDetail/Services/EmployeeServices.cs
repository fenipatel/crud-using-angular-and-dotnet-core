using System.Collections.Generic;
using NPoco;
using NPoco.FluentMappings;
using EmployeeDetail.Modals;
using EmployeeDetail.Services;
using System;

namespace EmployeeDetail.Services
{
    public class EmployeeServices : Iemployeeservices
    {
          public readonly IDatabase _db;

        public EmployeeServices(DatabaseFactory dbfactory){
            _db = dbfactory.GetDatabase();
        }

        public IEnumerable<Employee_portal> GetEmployee_portalData()
        {
            return _db.Fetch<Employee_portal>("select * from employee_portal order by id desc");
        }
        
         public bool SaveEmployeeData(Employee_portal emp)
        {
            var employee_portal = new Employee_portal()
            {
                
                FirstName = emp.FirstName,
                LastName = emp.LastName,
                Address = emp.Address,
                MobileNo = emp.MobileNo,
                Gender = emp.Gender,
                CreatedDate = DateTime.Now,
                ProfilePath = emp.ProfilePath,
                Status = emp.Status
                
            };
            var resultCount = _db.Insert(employee_portal);
            return true;
        }
        public bool UpdateEmployeedetails(Employee_portal emp)
        {
            var employee_portal = _db.SingleById<Modals.Employee_portal>(emp.Id);
            employee_portal.FirstName = emp.FirstName;
            employee_portal.LastName = emp.LastName;
            employee_portal.Address = emp.Address;
            employee_portal.MobileNo = emp.MobileNo;
            employee_portal.Gender = emp.Gender;
            employee_portal.ProfilePath = emp.ProfilePath;
            employee_portal.Status = emp.Status;

            var resultCount = _db.Update(employee_portal);
            return true;

        }

        public bool DeleteEmployeeDetails(long empid){
            var employee_portal = _db.SingleById<Modals.Employee_portal>(empid);
            var resultCount = _db.Delete(employee_portal);
            return true;
        }
    }
}