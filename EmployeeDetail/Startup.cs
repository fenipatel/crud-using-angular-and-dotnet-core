using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmployeeDetail.Modals;
using EmployeeDetail.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NPoco;
using NPoco.FluentMappings;

namespace EmployeeDetail {
    public class Startup {
        public Startup (IConfiguration configuration) {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices (IServiceCollection services) {
           
            services.AddControllers ();
            services.AddScoped<Iemployeeservices, EmployeeServices> ();
            var connection = Configuration.GetConnectionString ("Connection");
            var dbFactory = DatabaseFactory.Config (x => {
                x.UsingDatabase (() => new Database (connection, DatabaseType.SqlServer2012, System.Data.SqlClient.SqlClientFactory.Instance));
                x.WithFluentConfig (FluentMappingConfiguration.Configure (new NpocoMappings ()));
            });
            services.AddSingleton (dbFactory);
            
          
            var allowedOrigins = Configuration.GetSection ("AllowedOrigins").Get<List<string>> ().ToArray ();
            services.AddCors (options => {
                options.AddPolicy ("Pynora", builder => builder
                    .WithOrigins (allowedOrigins)
                    .AllowAnyMethod ()
                    .AllowAnyHeader ()
                    .AllowCredentials ());
            });     
        }

        public void Configure (IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment ()) {
                app.UseDeveloperExceptionPage ();
            }
            app.UseCors("Pynora"); 


            app.UseHttpsRedirection ();

            app.UseRouting ();

            app.UseAuthorization ();
           

            app.UseEndpoints (endpoints => {
                
                endpoints.MapControllers ();
          
            });
             
             
        }
      }
}