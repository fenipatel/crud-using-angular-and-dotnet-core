using NPoco;
using NPoco.FluentMappings;
namespace EmployeeDetail.Modals
{
    public class NpocoMappings : Mappings
    {
        public NpocoMappings () {
            For<Employee_portal> ().TableName ($"[dbo].[Employee_portal]");
        }
    }
}