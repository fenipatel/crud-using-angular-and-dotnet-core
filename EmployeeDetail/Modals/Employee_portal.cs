using System;

namespace EmployeeDetail.Modals
{   
    public class Employee_portal
    {
       
        public int Id { get; set; }
        public string FirstName { get; set; }
         public string LastName { get; set;} 
        public string Address { get; set; }
        public string MobileNo { get; set; }
        public string Gender { get; set; } 
        public DateTime? CreatedDate { get; set; }
         public string ProfilePath { get; set; } 
        public string Status { get; set; }  
    }
}