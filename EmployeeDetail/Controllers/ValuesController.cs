﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using EmployeeDetail.Services;
using Microsoft.AspNetCore.Cors;
using System.IO;
using System.Web;
using System.Net;
using System.Net.Http;


using EmployeeDetail.Modals;
namespace EmployeeDetail.Controllers
{
 
    [Route("api/[controller]")]
   
    [ApiController]
    [EnableCors("Pynora")]
    
    public class ValuesController : ControllerBase
    {


        public readonly Iemployeeservices _employeeservices;
        public ValuesController(Iemployeeservices employeeservices)
        {
            _employeeservices = employeeservices;
        }


        [HttpGet]
        public ActionResult GetEmployee_portal()
        {
            var results = _employeeservices.GetEmployee_portalData();
            return Ok(results);
        }
        

        
        [HttpPost]
             public bool Post([FromBody] Employee_portal emp)
        {    
            
            return (_employeeservices.SaveEmployeeData(emp));
        
        }
        [HttpPut]

        public bool Put([FromBody] Employee_portal emp)
        {
            var data = _employeeservices.UpdateEmployeedetails(emp);
            return data;
        }

        [HttpGet("deleteEmployeeDetails/{empid}")]
        public bool Delete(long empid)
        {
            return _employeeservices.DeleteEmployeeDetails(empid);
        }
    }
}

