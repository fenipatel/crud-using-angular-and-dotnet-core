import { Employee } from './Modals/Employee';

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
const httpPostOptions = {

    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    }),
    withCredentials: true,
};
@Injectable()
export class DataService {

constructor( private http : HttpClient) { }

GetEmployee(){
    return this.http.get('https://localhost:5001/api/values/');
}
GetPosts() {
    return this.http.get('https://localhost:5001/api/values/');
}
PostEmployee( emp : Employee , fileToUpload: File
    ){
    let data;
    data = this.http.post('https://localhost:5001/api/values/',
    emp, httpPostOptions);
    const formData: FormData = new FormData();
   formData.append('Image', fileToUpload, fileToUpload.name);
  return this.http
 .post(data, formData);
 //  return data;
}
//
UpdateEmployeeDetails(emp: Employee){
    let data;
    data = this.http.put('https://localhost:5001/api/values/',
                emp,httpPostOptions);
    return data;

    }

    //To Delete Employee Details
    DeleteEmployeeDetails(empid)
    {
        return this.http.get('https://localhost:5001/api/values/deleteEmployeeDetails/' + empid,httpPostOptions);

    }
}
