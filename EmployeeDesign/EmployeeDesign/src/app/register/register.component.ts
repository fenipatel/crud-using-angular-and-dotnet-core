
import { DataService } from './../Data.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { Employee } from '../Modals/Employee';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
 
})

///assets/image/default-image.png
export class RegisterComponent implements OnInit {
  imageUrl: string = "/assets/image/default-image.png";
  fileToUpload : File = null;
  constructor( private router : Router,
              private toastr : ToastrService,
              private service : DataService) { }
  Modals: Employee;
  modalref = BsModalRef;
  SaveProperty: string;
  IsAnyEdit: Boolean = false;

  
  public Item: any;
  //ValidateModel: ValidationModel;
  ngOnInit() {
    this.Modals = new Employee();
  }
  handleFileInput(file : FileList){
    this.fileToUpload = file.item(0);

    //image reader

    var reader = new FileReader();
    reader.onload = (event:any) =>{
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }

  // submitWritePost(methodfrom, openAsk){
  //   this.SaveProperty = methodfrom;
  //   let validateFlag = false;
  //   if(this.Item === undefined){
  //     this.IsAnyEdit = false;
  //   }
  //   commonSubmitQuestion(methodfrom, openAsk) {
  //     let input;
  //     input = new FormData();
  //     input.append('category', this.Model.category);
  //     input.append('title', this.Modals.title);
  //     if (this.Model.article != null) {
  //    }
  // }
  // }
 
  submitButton(EmployeeValues: any){
    this.service.PostEmployee(this.Modals,this.fileToUpload).subscribe(
      result => {
       debugger;
        console.log('done');
        Image = null;
            if(result === true){
              this.toastr.success("Data is saved successfully",'Success');
            }else{
              this.toastr.success("Data is not saved successfully" , 'Error');
            }
            this.imageUrl = "/assets/img/default-image.png";
            this.router.navigate(['/show']);
           
        });
      }
      
//
}
