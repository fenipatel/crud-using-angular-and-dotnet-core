
export class Employee {
    Id: number;
    FirstName : string ;
    LastName : string ;
    Address : string ;
    MobileNo : string;
    Gender : string;
    CreatedDate : string;
    ProfilePath : string;
    Status : string;
}
