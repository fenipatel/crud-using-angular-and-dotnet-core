import { Router } from '@angular/router';

import { ToastrModule, ToastrService } from 'ngx-toastr';
import { DataService } from './../Data.service';
import { Employee } from './../Modals/Employee';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { DataTablesModule } from 'angular-datatables';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {

  modalRef: BsModalRef;
  public empData: Object;
  public temp: Object=false;
  shows:object;
  Modals: Employee;
  message: string;
  deleteId:number;
  constructor( private toastr: ToastrService,
                private service: DataService,
                private modalService: BsModalService,
                private router: Router,
                private data: DataService) { }
  //Get Data from api
  ngOnInit() {
       this.Modals = new Employee();
      this.data.GetPosts().subscribe((data => {
      console.log(data);
      this.shows = data
               }),(resp: Response) =>{  
                this.empData = resp;
       this.temp = true;
       });
}
                // To create update pop up

 openUpdate(template: TemplateRef<any>, showsData){
      this.Modals.Id = showsData.id;
      this.Modals.FirstName = showsData.firstName;
      this.Modals.LastName = showsData.lastName;
      this.Modals.Address = showsData.address;
      this.Modals.MobileNo = showsData.mobileNo;
      this.Modals.CreatedDate = showsData.createdDate;
      this.Modals.ProfilePath = showsData.profilePath;
      this.Modals.Status = showsData.status;
      this.modalRef = this.modalService.show(template);   
}
//Update data 
submitButton(UpdateValues: any) : void{

  this.service.UpdateEmployeeDetails(this.Modals).subscribe(
    result =>{
      if(result == true){
        this.toastr.success("Data is Update successfully","Success");
      }else{
        this.toastr.success("Data is not update successfully","Error");
      }
      this.modalRef.hide();
      window.location.reload();
    });

}

//to open delete popup

    deleteDetails(template: TemplateRef<any>,postid)
    {
      this.deleteId = postid;
      this.modalRef = this.modalService.show(template,{class:'modal-sm'});
    }
 
    //to delete Employee Details
      confirm(empiid) : void{ 
        this.service.DeleteEmployeeDetails(empiid).subscribe(
          result =>{
            if(result == true){

            }
           window.location.reload();
          }
        )
        this.message = 'Confirmed';
        window.location.reload();
        this.modalRef.hide();
      }

      decline(): void{
        this.message = 'Declined';
        this.modalRef.hide();
      }
  
  

}
