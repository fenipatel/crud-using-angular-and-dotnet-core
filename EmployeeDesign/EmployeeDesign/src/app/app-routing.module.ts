import { ShowComponent } from './show/show.component';

import { RegisterComponent } from './register/register.component';

import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [{
    path : 'show',
    component: ShowComponent
  },{
    path: 'register',
   component: RegisterComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
